FROM golang:alpine AS builder

ADD ./ /go/src/gitlab.com/my-golang-hands-on/rest-music/rm

RUN set -ex && \
  cd /go/src/gitlab.com/my-golang-hands-on/rest-music/rm && \
  CGO_ENABLED=0 go build \
        -tags netgo \
        -v -a \
        -ldflags '-extldflags "-static"' && \
  mv ./rm /usr/bin/rm

FROM busybox

COPY --from=builder /usr/bin/rm /usr/local/bin/rm

# Set the binary as the entrypoint of the container
ENTRYPOINT [ "rm" ]