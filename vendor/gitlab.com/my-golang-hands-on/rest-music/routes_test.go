package mtrest

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

func ExampleGetAllKeysLinks() {
	fmt.Println(GetAllKeysLinks(0))
	fmt.Println(GetAllKeysLinks(2))
	fmt.Println(GetAllKeysLinks(11))
	// Output:
	// [{/keys/0 singleKey GET} {/keys/1 next GET}]
	// [{/keys/2 singleKey GET} {/keys/1 prev GET} {/keys/3 next GET}]
	// [{/keys/11 singleKey GET} {/keys/10 prev GET}]
}

func TestCreateAllKeysHandler(t *testing.T) {
	route := CreateAllKeysHandler(Model)
	router := mux.NewRouter()
	router.HandleFunc("/keys", route)
	request, error := http.NewRequest("GET", "/keys", nil)
	if error != nil {
		fmt.Println(error)
	}
	resp := httptest.NewRecorder()
	route.ServeHTTP(resp, request)
	if http.StatusOK != resp.Code {
		t.Errorf("expected 200 got %d", resp.Code)
	}
	var k AllKeys
	readBuf, _ := ioutil.ReadAll(resp.Body)
	error = json.Unmarshal(readBuf, &k)

	if len(k.Keys[0].Links) != 2 {
		t.Errorf("expected first element to have 2 links, got %d", len(k.Keys[0].Links))
	}
	if len(k.Keys[11].Links) != 2 {
		t.Errorf("expected first element to have 2 links, got %d", len(k.Keys[0].Links))
	}
	if len(k.Keys[2].Links) != 3 {
		t.Errorf("expected first element to have 3 links, got %d", len(k.Keys[0].Links))
	}
	if k.Keys[0].Tonic != `C` {
		t.Errorf("expected first element to have C as tonic, got %s", k.Keys[0].Tonic)
	}
}

func TestCreateKeyInfoHandler(t *testing.T) {
	route := CreateKeyInfoHandler(Model)
	router := mux.NewRouter()
	router.HandleFunc("/keys/{position}", route)
	//router.HandleFunc("/keys/{position}", route)
	request, error := http.NewRequest("GET", "/keys/0", nil)

	if error != nil {
		fmt.Println(error)
	}
	request = mux.SetURLVars(request, map[string]string{
		`position`: `0`,
	})
	resp := httptest.NewRecorder()
	route.ServeHTTP(resp, request)
	if http.StatusOK != resp.Code {
		t.Errorf("expected 200 got %d", resp.Code)
	}
	var k KeyInfo
	readBuf, _ := ioutil.ReadAll(resp.Body)
	error = json.Unmarshal(readBuf, &k)
	if len(k.Links) != 2 {
		t.Errorf("expected 2 links got %d", len(k.Links))
	}
	if k.Links[1].Href != `/keys/9` {
		t.Errorf("expected /keys/9 links got %s", k.Links[1].Href)
	}

	request, error = http.NewRequest("GET", "/keys/13", nil)
	request = mux.SetURLVars(request, map[string]string{
		`position`: `13`,
	})
	resp = httptest.NewRecorder()
	route.ServeHTTP(resp, request)
	if http.StatusNotFound != resp.Code {
		t.Errorf("expected 404 got %d", resp.Code)
	}

	request, error = http.NewRequest("GET", "/keys/4", nil)
	request = mux.SetURLVars(request, map[string]string{
		`position`: `4`,
	})
	resp = httptest.NewRecorder()
	route.ServeHTTP(resp, request)
	if http.StatusOK != resp.Code {
		t.Errorf("expected 200 got %d", resp.Code)
	}
	readBuf, _ = ioutil.ReadAll(resp.Body)
	error = json.Unmarshal(readBuf, &k)
	if len(k.Links) != 3 {
		t.Errorf("expected 3 links got %d", len(k.Links))
	}
	if k.Links[2].Href != `/keys/1` {
		t.Errorf("expected /keys/9 links got %s", k.Links[2].Href)
	}

	request, error = http.NewRequest("GET", "/keys/11", nil)
	request = mux.SetURLVars(request, map[string]string{
		`position`: `11`,
	})
	resp = httptest.NewRecorder()
	route.ServeHTTP(resp, request)
	if http.StatusOK != resp.Code {
		t.Errorf("expected 200 got %d", resp.Code)
	}
	readBuf, _ = ioutil.ReadAll(resp.Body)
	error = json.Unmarshal(readBuf, &k)
	if len(k.Links) != 2 {
		t.Errorf("expected 2 links got %d", len(k.Links))
	}
	if k.Links[1].Href != `/keys/8` {
		t.Errorf("expected /keys/8 links got %s", k.Links[1].Href)
	}

	request, error = http.NewRequest("GET", "/keys/bla", nil)
	request = mux.SetURLVars(request, map[string]string{
		`position`: `bla`,
	})
	resp = httptest.NewRecorder()
	route.ServeHTTP(resp, request)
	if http.StatusBadRequest != resp.Code {
		t.Errorf("expected 400 got %d", resp.Code)
	}
}
