package mtrest

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// Link defines the Link part of a response
type Link struct {
	Href string `json:"href"`
	Rel  string `json:"rel"`
	Verb string `json:"type"`
}

type allKeysKey struct {
	MtKey
	Links []Link
}

// AllKeys defines the structure of the response of /keys
type AllKeys struct {
	Count int          `json:"count"`
	Keys  []allKeysKey `json:"keys"`
	Links []Link       `json:"links"`
}

// KeyInfo defines the structure of the response of /keys/{position}
type KeyInfo struct {
	MtKeyInfo
	Links []Link `json:"links"`
}

// CreateAllKeysHandler is a factory function to create a handler function for handling the /keys request
func CreateAllKeysHandler(m IKeysModel) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		model := m
		keys := model.GetAllMajorKeys()
		var ret []allKeysKey
		for i, v := range keys {
			akk := allKeysKey{
				MtKey: v,
				Links: GetAllKeysLinks(i),
			}
			ret = append(ret, akk)
		}
		resp := AllKeys{
			Count: len(ret),
			Keys:  ret,
			Links: []Link{
				{
					Href: `/keys`,
					Rel:  `self`,
					Verb: `GET`,
				},
				{
					Href: `/keys/0`,
					Rel:  `first`,
					Verb: `GET`,
				},
			},
		}
		json, _ := json.MarshalIndent(resp, "", "  ")
		w.Write(json)
	}
}

// GetAllKeysLinks is a helper function to create links on key level AllKeys
func GetAllKeysLinks(pos int) []Link {
	var links []Link
	links = append(links, Link{
		Href: fmt.Sprintf("/keys/%d", pos),
		Rel:  `singleKey`,
		Verb: `GET`,
	})
	if pos > 0 {
		links = append(links, Link{
			Href: fmt.Sprintf("/keys/%d", pos-1),
			Rel:  `prev`,
			Verb: `GET`,
		})
	}
	if pos < 11 {
		links = append(links, Link{
			Href: fmt.Sprintf("/keys/%d", pos+1),
			Rel:  `next`,
			Verb: `GET`,
		})
	}
	return links
}

// GetKeyInfoLinks is a helper function to create links on key level for KeyInfo
func GetKeyInfoLinks(pos int) []Link {
	var links []Link
	var rel int
	offset := 3
	if pos > 2 {
		rel = pos - offset
	} else {
		rel = 12 - offset + pos
	}
	if pos > 0 {
		links = append(links, Link{
			Href: fmt.Sprintf("/keys/%d", pos-1),
			Rel:  `prev`,
			Verb: `GET`,
		})
	}
	if pos < 11 {
		links = append(links, Link{
			Href: fmt.Sprintf("/keys/%d", pos+1),
			Rel:  `next`,
			Verb: `GET`,
		})
	}
	links = append(links, Link{
		Href: fmt.Sprintf("/keys/%d", rel),
		Rel:  `Parallel`,
		Verb: `GET`,
	})
	return links
}

// CreateKeyInfoHandler is a factory function to create a hander for /keys/{position}
func CreateKeyInfoHandler(m IKeysModel) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id, err := strconv.Atoi(vars["position"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if id < 0 || id > 11 {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		key := m.GetKeyByPosition(id)

		resp := KeyInfo{
			key,
			GetKeyInfoLinks(id),
		}
		json, _ := json.MarshalIndent(resp, "", "  ")
		w.Write(json)
	}
}
