package mtrest

import (
	musictheory "gitlab.com/my-golang-hands-on/music-theory"
)

// MtKey defines a key to be part of the return at route /keys
type MtKey struct {
	Position int    `json:"position"`
	Tonic    string `json:"tonic"`
}

// MtKeyInfo represents a single KeyInfo get by /keys/{poistion}
type MtKeyInfo struct {
	Position int      `json:"position"`
	Tonic    string   `json:"tonic"`
	Tonality string   `json:"type"`
	Signs    []string `json:"signs"`
	Scale    []string `json:"scale"`
	Minor    string   `json:"minor"`
}

// IKeysModel defines the necessary methods a model has to implement
type IKeysModel interface {
	GetAllMajorKeys() []MtKey
	GetKeyByPosition(pos int) MtKeyInfo
	// GetRelative(pos int) MtKey
	// GetParallel(pos int) MtKey
}

// DefaultModel defines a default model if no other model is provided
type DefaultModel struct {
	CoF musictheory.ICoF
}

// Model is the default model for this service
var Model = DefaultModel{
	musictheory.GetCoF(),
}

// GetAllMajorKeys is a method implementation of the IKeysModel interface
func (dm DefaultModel) GetAllMajorKeys() []MtKey {
	keys := dm.CoF.GetMajorRing().GetAllKeys()
	res := make([]MtKey, 12)
	for i, v := range keys {
		mt := MtKey{
			Position: v.Position,
			Tonic:    v.Tonic,
		}
		res[i] = mt
	}
	return res
}

// GetKeyByPosition is method implementation of IKeysModel interface for getting a single key
func (dm DefaultModel) GetKeyByPosition(pos int) MtKeyInfo {
	if pos < 0 || pos > 11 {
		pos = 0
	}
	k := dm.CoF.GetMajorRing().GetKey(pos)
	ret := MtKeyInfo{
		Position: pos,
		Tonic:    k.Tonic,
		Tonality: `Major`,
		Signs:    k.Signs,
		Scale:    k.Scale.Notes,
		Minor:    dm.CoF.GetMinorRing().GetKey(pos).Tonic,
	}
	return ret
}
