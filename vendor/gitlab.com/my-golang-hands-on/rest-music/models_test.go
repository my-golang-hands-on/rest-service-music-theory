package mtrest

import (
	"fmt"
)

func ExampleDefaultModel_GetAllMajorKeys() {
	k := Model.GetAllMajorKeys()
	fmt.Println(k)
	// Output:
	// [{0 C} {1 G} {2 D} {3 A} {4 E} {5 B} {6 F♯} {7 D♭} {8 A♭} {9 E♭} {10 B♭} {11 F}]
}

func ExampleDefaultModel_GetKeyByPosition() {
	k := Model.GetKeyByPosition(0)
	fmt.Println(k)
	k = Model.GetKeyByPosition(9)
	fmt.Println(k)
	k = Model.GetKeyByPosition(99)
	fmt.Println(k)
	// Output:
	//{0 C Major [] [C D E F G A B] A}
	// {9 E♭ Major [B♭ E♭ A♭] [E♭ F G A♭ B♭ C D] C}
	// {0 C Major [] [C D E F G A B] A}
}
