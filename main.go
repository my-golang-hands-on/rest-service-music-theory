package main

import (
	"net/http"

	"github.com/gorilla/mux"

	mtrest "gitlab.com/my-golang-hands-on/rest-music"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/keys", mtrest.CreateAllKeysHandler(mtrest.Model))
	r.HandleFunc("/keys/{position}", mtrest.CreateKeyInfoHandler(mtrest.Model))
	http.ListenAndServe(":8080", r)
}
