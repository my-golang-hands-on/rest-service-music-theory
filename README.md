# Rest Service "Music Theory"

[![pipeline status](https://gitlab.com/my-golang-hands-on/rest-service-music-theory/badges/master/pipeline.svg)](https://gitlab.com/my-golang-hands-on/rest-service-music-theory/commits/master)
[![coverage report](https://gitlab.com/my-golang-hands-on/rest-service-music-theory/badges/master/coverage.svg)](https://gitlab.com/my-golang-hands-on/rest-service-music-theory/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Api Version](https://img.shields.io/badge/OpenApi-3.0-yellowgreen.svg)](https://gitlab.com/my-golang-hands-on/rest-service-music-theory/raw/master/music-theory-1.0.0.yaml)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/my-golang-hands-on/rest-service-music-theory)](https://goreportcard.com/report/gitlab.com/my-golang-hands-on/rest-service-music-theory)

To run/build this Service two additional packages are required

- github.com/gorilla/mux
- [gitlab.com/my-golang-hands-on/music-theory](https://gitlab.com/my-golang-hands-on/music-theory) which is one of my other projects

This package creates a simple REST-Service with HATEOAS-functionality to return informations about keys of the circle of fifths.
The OpenApi 3.0 definition for this API can be found on [swagger-doc for this project](https://app.swaggerhub.com/apis-docs/hapevau/music-theory/1.0.0)

Below is the documentation of the implementation of that definition in Golang.

There is also a [docker image](https://gitlab.com/my-golang-hands-on/rest-service-music-theory/container_registry) (size 5MB) available. See the [wiki](https://gitlab.com/my-golang-hands-on/rest-service-music-theory/wikis/rest-music) on how to use it or how to build a image based on the Dockerfile of that project.

## VARIABLES

### Model

```go
var Model = DefaultModel{
    musictheory.GetCoF(),
}
```

**Model** is the default model for this service

## FUNCTIONS

### CreateAllKeysHandler

```go
func CreateAllKeysHandler(m IKeysModel) http.HandlerFunc
```

**CreateAllKeysHandler** is a factory function to create a handler function for handling the /keys request

### CreateKeyInfoHandler

```go
func CreateKeyInfoHandler(m IKeysModel) http.HandlerFunc
```

**CreateKeyInfoHandler** is a factory function to create a hander for /keys/{position}

## TYPES

### AllKeys

```go
type AllKeys struct {
    Count int          `json:"count"`
    Keys  []allKeysKey `json:"keys"`
    Links []Link       `json:"links"`
}
```

**AllKeys** defines the structure of the response of /keys

### DefaultModel

```go
type DefaultModel struct {
    CoF musictheory.ICoF
}
```

**DefaultModel** defines a default model if no other model is provided

### GetAllMajorKeys

```go
func (dm DefaultModel) GetAllMajorKeys() []MtKey
```

**GetAllMajorKeys** is a method implementation of the IKeysModel Interface

*Example:*

```go
k := Model.GetAllMajorKeys()
fmt.Println(k)
```

*Output:*

```bash
[{0 C} {1 G} {2 D} {3 A} {4 E} {5 B} {6 F♯} {7 D♭} {8 A♭} {9 E♭} {10 B♭} {11 F}]
```

### GetKeyByPosition

```go
func (dm DefaultModel) GetKeyByPosition(pos int) MtKeyInfo
```

**GetKeyByPosition** is method implementation of IKeysModel interface for getting a single key

*Example:*

```go
k := Model.GetKeyByPosition(0)
fmt.Println(k)
k = Model.GetKeyByPosition(9)
fmt.Println(k)
k = Model.GetKeyByPosition(99)
fmt.Println(k)
```

*Output:*

```bash
{0 C Major [] [C D E F G A B] A}
{9 E♭ Major [B♭ E♭ A♭] [E♭ F G A♭ B♭ C D] C}
{0 C Major [] [C D E F G A B] A}
```

### IKeysModel

```go
type IKeysModel interface {
    GetAllMajorKeys() []MtKey
    GetKeyByPosition(pos int) MtKeyInfo
}
```

**IKeysModel** defines the necessary methods a model has to implement

### KeyInfo

```go
type KeyInfo struct {
    MtKeyInfo
    Links []Link `json:"links"`
}
```

*KeyInfo* defines the structure of the response of /keys/{position}

### Link

```go
type Link struct {
    Href string `json:"href"`
    Rel  string `json:"rel"`
    Verb string `json:"type"`
}
```

**Link** defines the Link part of a response

### GetAllKeysLinks

```go
func GetAllKeysLinks(pos int) []Link
```

**GetAllKeysLinks** is a helper function to create links on key level

*Example:*

```go
fmt.Println(GetAllKeysLinks(0))
fmt.Println(GetAllKeysLinks(2))
fmt.Println(GetAllKeysLinks(11))
```

*Output:*

```bash
[{/keys/0 singleKey GET} {/keys/1 next GET}]
[{/keys/2 singleKey GET} {/keys/1 prev GET} {/keys/3 next GET}]
[{/keys/11 singleKey GET} {/keys/10 prev GET}]
```

### GetKeyInfoLinks

```go
func GetKeyInfoLinks(pos int) []Link
```

*GetKeyInfoLinks* is a helper function to create links on key level for  KeyInfo

### MtKey

```go
type MtKey struct {
    Position int    `json:"position"`
    Tonic    string `json:"tonic"`
}
```

**MtKey** defines a key to be part of the return at route /keys

### MtKeyInfo

```go
type MtKeyInfo struct {
    Position int      `json:"position"`
    Tonic    string   `json:"tonic"`
    Tonality string   `json:"type"`
    Signs    []string `json:"signs"`
    Scale    []string `json:"scale"`
}
```

**MtKeyInfo** represents a single KeyInfo get by /keys/{poistion}
